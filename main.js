const concurrently = require('concurrently');

const processes = [
  {
    name: 'SERVER',
    command: 'npm run start-server',
    prefixColor: 'bgBlue.bold',
    env: {},
  },
  {
    name: 'CLIENT',
    command: 'npm run start-client',
    prefixColor: 'bgMagenta.bold',
    env: {},
  },
];

concurrently(processes, {
  killOthers: ['failure', 'success'],
  prefix: '[{name}]',
})
  .then(() => {
    console.log('Successfully started concurrently processes of');

    processes.forEach((process) => {
      console.log(`\t[${process.name}] -> ${process.command}`);
    });
  })
  .catch(() => {
    console.log('An error occured while starting processes of');

    processes.forEach((process) => {
      console.log(`\t[${process.name}] -> ${process.command}`);
    });
  })
  .finally(() => {
    // Nothing to do here
  });
