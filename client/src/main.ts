// Vue
import Vue from 'vue';

// Bulma
import 'bulma';

// Bulma badge
import '@creativebulma/bulma-badge/dist/bulma-badge.css';

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

library.add(fas);
library.add(far);

Vue.component('font-awesome-icon', FontAwesomeIcon);

// Crée l'instance principale de Vue
const app = new Vue({
  router,
  store,
  render: (h) => h(App),
});

// Monte l'instance de Vue
app.$mount('#app');
