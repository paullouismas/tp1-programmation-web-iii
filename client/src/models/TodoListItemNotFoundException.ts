/**
 * Classe d'erreur lors d'une tentative d'accès à une tâche inexistante.
 *
 * @class
 *
 * @extends {Error} - Étend la classe d'erreur de base.
 *
 * @author Paul-Louis Mas
 */
export default class TodoListItemNotFoundException extends Error {
  /** L'id de tâche demandé */
  public requestedId: string;

  /**
   * Crée une nouvelle erreur.
   *
   * @constructor
   *
   * @param {String} requestedId - L'`id` de la tâche demandée.
   */
  public constructor(requestedId: string) {
    super();

    // Assigne l'id de tâche demandé
    this.requestedId = requestedId;
  }
}
