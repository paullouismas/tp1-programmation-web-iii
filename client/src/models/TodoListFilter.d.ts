/**
 * Interface de filtrage de tâches.
 *
 * @interface
 *
 * @author Paul-Louis Mas
 */
export interface TodoListFilter {
  /** La propriété sur laquel trier les tâches */
  order: '' | 'name' | 'completed';

  /** Un filtre textuel à appliquer sur le nom des tâches */
  filter: string;

  /** Le numéro de page */
  page: number;

  /** Nombre de tâches maximale par pages */
  maxTodosPerPage: number;
}
