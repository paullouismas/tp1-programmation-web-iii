import * as uuid from 'uuid';

import { TodoListItem } from './TodoListItem';
import { TodoListItemData } from './TodoListItemData';

/**
 * Classe représentant une tâche dans l'application.
 *
 * @class
 *
 * @implements {TodoListItem} - Implémente l'interface d'une tâche.
 *
 * @author Lincey Jérôme
 */
export default class Todo implements TodoListItem {
  /** L'`id` de la tâche */
  public id: string;

  /** L'état de la tâche (complété ou non) */
  public completed: boolean;

  /** L'échéance de la tâche */
  public dueDate: string | null;

  /** Le nom de la tâche */
  public name: string;

  /** La description de la tâche */
  public description: string;

  /**
   * Crée une nouvelle tâche.
   *
   * @constructor
   *
   * @param {Object} task - Les propriétés de la tâche.
   * @param {String | null} task.dueDate - L'échéance de la tâche.
   * @param {String} task.name - Le nom de la tâche.
   * @param {String} task.description - La description de la tâche.
   */
  constructor({ dueDate, name, description }: TodoListItemData) {
    // Assigne un id pseudo-aléatoire et unique
    this.id = uuid.v4();

    // Une tâche n'est jamais complété lors de sa création
    this.completed = false;

    // La date due au format "yyyy-MM-ddThh:mm" ou null s'il n'y en à pas.
    this.dueDate = dueDate || null;

    // Le nom de la tâche, visible en format réduit
    this.name = name;

    // La description de la tâche, visible lors de l'ouverture en détail de la tâche
    this.description = description || '';
  }
}
