import Todo from './Todo';
import { TodoListPagination } from './TodoListPagination';

/**
 * Interface définissant le retour de la liste de tâches depuis l'API.
 *
 * @interface
 *
 * @author Paul-Louis Mas
 */
export interface TodoListReturn {
  /** Si la liste retournée est tronquée */
  truncated: boolean;

  /** La liste de tâches retournées */
  todos: Todo[];

  /** La pagination */
  pagination: TodoListPagination;

  /** Le nombre de tâches au totat */
  resultCount: number;
}
