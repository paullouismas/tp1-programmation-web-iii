/**
 * Interface définissant l'état d'une tâche.
 *
 * @interface
 *
 * @author Paul-Louis Mas
 */
export interface TodoListItemState {
  /** L'`id` de la tâche */
  id: string;

  /** L'état (complété ou non) de la tâche */
  completed: boolean;
}
