/**
 * Interface des données d'une tâche.
 *
 * @interface
 *
 * @author Paul-Louis Mas
 */
export interface TodoListItemData {
  /** L'échéance de la tâche */
  dueDate?: string;

  /** Le nom de la tâche */
  name: string;

  /** La description de la tâche */
  description?: string;
}
