/**
 * Interface définissant la pagination.
 *
 * @interface
 *
 * @author Paul-Louis Mas
 */
export interface TodoListPagination {
  /** La première page */
  first: number;

  /** La page précédente */
  previous?: number;

  /** La page actuelle */
  current: number;

  /** La page suivante */
  next?: number;

  /** La dernière page */
  last: number;
}
