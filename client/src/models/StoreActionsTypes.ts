/**
 * Enum définissant la liste des actions du Store.
 *
 * @enum
 *
 * @author Paul-Louis Mas
 */
enum StoreActionsTypes {
  /** Création d'une tâche */
  CREATE_TODO = 'createTodo',

  /** Récupération d'une tâche */
  GET_TODO = 'getTodo',

  /** Récupération de toutes les tâches */
  LOAD_TODOS = 'loadTodos',

  /** Modification d'une tâche */
  UPDATE_TODO = 'updateTodo',

  /** Suppression d'une tâche */
  DELETE_TODO = 'deleteTodo',
}

export default StoreActionsTypes;
