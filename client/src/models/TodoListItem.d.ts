import { TodoListItemData } from './TodoListItemData';
import { TodoListItemState } from './TodoListItemState';

/**
 * Interface d'une tâche.
 *
 * @interface
 *
 * @extends {TodoListItemData} - Étend l'interface des données d'une tâche.
 * @extends {TodoListItemState} - Étend l'interface de l'état d'une tâche.
 *
 * @author Paul-Louis Mas
 */
export interface TodoListItem extends TodoListItemData, TodoListItemState {
  /** L'`id` de la tâche */
  id: string;

  /** L'état (complété ou non) de la tâche */
  completed: boolean;

  /** L'échéance de la tâche */
  dueDate: string | null;

  /** Le nom de la tâche */
  name: string;

  /** La description de la tâche */
  description: string;
}
