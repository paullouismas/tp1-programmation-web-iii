import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

import Todo from '@/models/Todo';
import { TodoListFilter } from '@/models/TodoListFilter';
import { TodoListReturn } from '@/models/TodoListReturn';
import TodoListItemNotFoundException from '@/models/TodoListItemNotFoundException';

Vue.use(Vuex);

/** L'adresse de l'API */
const API_BASE = 'http://localhost:3000/api/';
/** La configuration Axios */
const axiosInstance = axios.create({
  baseURL: API_BASE,
  headers: {
    'X-Source': 'TodoList.client',
  },
  timeout: 10000, // 10s
  responseType: 'json',
});

/**
 * La configuration du Store pour la gestion des données.
 *
 * @constant
 *
 * @author Paul-Louis Mas
 */
export default new Vuex.Store({
  actions: {
    /**
     * Action pour créer une tâche.
     *
     * @author François Morel
     */
    async createTodo(_context, { id, completed, dueDate, name, description }: Todo) {
      try {
        // Requête de création de la tâche
        const response = await axiosInstance.post<Todo>('todos', {
          id,
          completed,
          dueDate,
          name,
          description,
        });
        // Retour de la réponse qui équivaux à la tâche nouvellement créé
        return response.data;
      } catch (error) {
        // Envoie l'erreur pour l'affichage côté client
        throw new Error(error);
      }
    },

    /**
     * Action pour récupérer une tâche depuis l'API.
     *
     * @author Paul-Louis Mas
     */
    async getTodo(_context, id: string) {
      try {
        /** La réponse de l'API pour récupérer une tâche depuis son id */
        const response = await axiosInstance.get<Todo>(`todos/${id}`);

        // Retourne la tâche
        return response.data;
      } catch (error) {
        // Une erreur est lancé puisqu'axios n'a pas été capable d'effectuer la requête

        if (error.response.status === 404) {
          // La tâche n'existe pas, lance une exception
          throw new TodoListItemNotFoundException(id);
        } else {
          // Propage l'erreur
          throw new Error(error);
        }
      }
    },

    /**
     * Action pour récupérer les tâches depuis l'API.
     *
     * @author Paul-Louis Mas
     */
    async loadTodos(
      _context,
      {
        filter = '', // Le filtre textuelle
        order = '', // L'ordre par lequel est trié les tâches
        page = 1, // Page actuelle
        maxTodosPerPage = 20, // Nombre de tâches maximum par page
      }: TodoListFilter,
    ) {
      try {
        /** La réponse de l'API pour récupérer la liste des tâches */
        const response = await axiosInstance.get<TodoListReturn>('todos', {
          params: {
            filter, // Le filtre textuelle
            order, // Nom de la propriété sur laquelle trier
            limit: maxTodosPerPage, // Nombre maximum de tâches retournées
            page, // L'index de page
          },
        });

        return response.data;
      } catch (error) {
        // Propage l'erreur
        throw new Error(error);
      }
    },

    /**
     * Action pour modifier une tâche.
     *
     * @author Simon Castonguay
     */
    async updateTodo(_context, { id, completed, dueDate, name, description }: Todo) {
      try {
        // Effectue une requête HTTP PUT pour modifier la tâche

        const response = await axiosInstance.put<Todo>(`todos/${id}`, {
          id,
          completed,
          dueDate,
          name,
          description,
        } as Todo);

        return response.data;
      } catch (error) {
        // Une erreur est lancé puisqu'axios n'a pas été capable d'effectuer la requête

        if (error.response.status === 404) {
          // la tâche n'existe pas', lance une exception
          throw new TodoListItemNotFoundException(id);
        } else {
          // Propage l'erreur
          throw new Error(error);
        }
      }
    },

    /**
     * Action pour supprimer une tâche.
     *
     * @author Lincey Jérôme
     */
    async deleteTodo(_context, id: string) {
      try {
        // Effectue une requête HTTP DELETE pour supprimer une tâche depuis son id
        await axiosInstance.delete<{}>(`todos/${id}`);
        // Retourne un booléen indiquant le succès
        return true;
      } catch (error) {
        // Une erreur est lancé puisqu'axios n'a pas été capable d'effectuer la requête
        if (error.response.status === 404) {
          // la tâche n'existe pas, lance une exception
          throw new TodoListItemNotFoundException(id);
        } else {
          // Propage l'erreur
          throw new Error(error);
        }
      }
    },
  },
});
