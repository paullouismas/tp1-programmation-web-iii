import Vue from 'vue';
import VueRouter from 'vue-router';

import TaskList from '@/views/tasklist/TaskList.vue';

Vue.use(VueRouter);

/**
 * Le routage de l'application.
 *
 * @constant
 *
 * @author Paul-Louis Mas
 */
const router = new VueRouter({
  routes: [
    /* Route d'accueil, c'est la racine de l'application */
    {
      path: '/list/:page?', // L'url actuelle de la route
      name: 'Home', // Le nom d'identification de la route
      component: TaskList, // Le component utilisé par la route
      meta: {
        title: 'Liste des tâches', // Spécifie le titre de la route
      },
      alias: [
        // Les autres URLs qui pointent vers cette même route
        '/',
      ],
    },

    /* Route pour la création d'une tâche, Create du CRUD */
    {
      path: '/create/',
      name: 'Create',
      component: () => import(/* webpackChunkName: 'Create' */ '@/views/create/CreateTask.vue'),
      meta: {
        title: "Création d'une nouvelle tâche",
      },
    },

    /* Route pour la lecture d'une tâche, Read du CRUD */
    {
      path: '/read/:id',
      name: 'Read',
      component: () => import(/* webpackChunkName: 'Read' */ '@/views/read/ReadTask.vue'),
      meta: {
        title: 'Information sur la tâche',
      },
    },

    /* Route pour la modification d'une tâche, Update du CRUD */
    {
      path: '/update/:id',
      name: 'Update',
      component: () => import(/* webpackChunkName: 'Update' */ '@/views/update/UpdateTask.vue'),
      meta: {
        title: 'Modification de la tâche',
      },
    },

    /* Route pour la suppression d'une tâche, Delete du CRUD */
    {
      path: '/delete/:id',
      name: 'Delete',
      component: () => import(/* webpackChunkName: 'Delete' */ '@/views/delete/DeleteTask.vue'),
      meta: {
        title: 'Suppression de la tâche',
      },
    },

    /* Route pour erreur 404 */
    {
      path: '*',
      name: '404',
      component: () => import(/* webpackChunkName: '404' */ '@/views/404/404.vue'),
      meta: {
        title: 'Erreur 404 - Page introuvable',
      },
    },
  ],
  linkExactActiveClass: 'is-active',
});

export default router;
