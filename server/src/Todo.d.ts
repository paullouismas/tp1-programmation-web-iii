/**
 * Interface représentant une tâche.
 *
 * @interface
 */
export interface ITodo extends Record<string, string | boolean | null> {
  /** L'id de la tâche */
  id: string;
  /** L'état (complété ou non) de la tâche */
  completed: boolean;
  /** La date à laquel la tâche est due, ou `null` si elle n'a pas d'échéance */
  dueDate: string | null;
  /** Le nom de la tâche */
  name: string;
  /** La description de la tâche */
  description: string;
}
