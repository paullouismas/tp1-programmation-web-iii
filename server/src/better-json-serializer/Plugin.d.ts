export interface IPlugin {
  /** La fonction pour sérializer l'objet */
  serialize(key: string, value: unknown): unknown;
  /** La fonction pour désirializer l'objet */
  deserialize(key: string, value: unknown): unknown;
}
