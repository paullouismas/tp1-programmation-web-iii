export interface SerializedObject {
  '_@serialized-object': {
    '_@serialized-object.type': string;
    '_@serialized-object.value': unknown;
  };
}
