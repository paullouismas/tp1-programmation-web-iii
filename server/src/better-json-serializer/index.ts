import { IPlugin } from './Plugin';
import { SerializedObject } from './SerializerObject';

/**
 * Une classe pour faciliter la sérialization et désérialization d'objets non standard en JSON.
 *
 * @author Paul-Louis Mas
 */
class BetterJSONSerializer {
  /** Liste des plugins */
  private static plugins = new Map<string, IPlugin>([
    /** Plugin pour les objets `Set` */
    [
      'Set',
      {
        serialize: (_key, value) => Array.from(value as Set<unknown>),
        deserialize: (_key, value) => new Set(value as Set<unknown>),
      },
    ],

    /** Plugin pour les objets `Map` */
    [
      'Map',
      {
        serialize: (_key, value) => Array.from(value as Map<unknown, unknown>),
        deserialize: (_key, value) => new Map(value as Map<unknown, unknown>),
      },
    ],
  ]);

  /**
   * Ajoute un plugin dans la liste de plugins.
   *
   * @param {String} constructorName - Le constructeur qui match ce plugin.
   * @param {IPlugin} plugin - Le plugin à ajouter.
   * @param {(key: String, value: any) => any} plugin.serialize - La fonction pour sérializer
   *   l'objet.
   * @param {(key: String, value: any) => any} plugin.deserialize - La fonction pour désérializer
   *   l'objet.
   */
  public static use(constructorName: string, plugin: IPlugin): void {
    // Ajoute le plugin dans la liste des plugins
    this.plugins.set(constructorName, plugin);
  }

  /**
   * Fonction appelé pour convertir un objet en JSON.
   *
   * @param {*} source - L'objet à convertir en JSON.
   * @param {number} [space=0] - Le nombre d'espaces à utiliser pour aérer le code JSON.
   *
   * @returns {String} - L'objet JSON.
   */
  public static stringify<C>(source: C, space = 0): string {
    /** L'objet JSON */
    const json = JSON.stringify(
      source,
      (key, value) => {
        if (!value) {
          return value;
        }

        /** Le nom du constructeur de `value` */
        const constructorName: string = value.constructor.name;

        /** Le plugin à utiliser */
        const matchingPlugin = this.plugins.get(constructorName);

        if (!matchingPlugin) {
          return value;
        }

        // Retourne l'objet sérializé
        return {
          '_@serialized-object': {
            '_@serialized-object.type': constructorName,
            '_@serialized-object.value': matchingPlugin.serialize(key, value),
          },
        } as SerializedObject;
      },
      space,
    );

    return json;
  }

  /**
   * Fonction appelé pour convertir du JSON en objet.
   *
   * @param {String} source - Le JSON à convertir en objet.
   *
   * @returns {Object} - L'objet provenant du code JSON.
   */
  public static parse<O>(source: string): O {
    /** L'objet provenant du JSON */
    const object = JSON.parse(source, (key, value) => {
      if (typeof value !== 'object' || value === null || !('_@serialized-object' in value)) {
        return value;
      }

      /** L'objet sérializé */
      const serializedObject = (value as SerializedObject)['_@serialized-object'];

      /** Le nom du constructeur de l'objet */
      const constructorName = serializedObject['_@serialized-object.type'];

      /** Le plugin à utiliser */
      const matchingPlugin = this.plugins.get(constructorName);

      if (!matchingPlugin) {
        return serializedObject['_@serialized-object.value'];
      }

      // Retourne l'objet désérializé
      return matchingPlugin.deserialize(key, serializedObject['_@serialized-object.value']);
    });

    return object;
  }
}

export default BetterJSONSerializer;
