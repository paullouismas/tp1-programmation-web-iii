import DatabaseException from './DatabaseException';

/**
 * Erreur lancée lors d'une erreur dans l'exécution d'un prédicat sur la base de donnée.
 *
 * @class
 *
 * @extends {DatabaseException} - Étend la classe d'erreur général de base de donnée.
 *
 * @author Paul-Louis Mas
 */
export default class DatabasePredicateException extends DatabaseException {
  /**
   * @param {String} dbFile - Le fichier de base de donnée.
   * @param {any[]} args - Les arguments de l'erreur originale.
   */
  constructor(dbFile: string, ...args: string[]) {
    super(dbFile, ...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DatabasePredicateException);
    }

    /** Le nom de l'erreur */
    this.name = 'DatabasePredicateException';

    /** Le fichier de base de donnée */
    this.dbFile = dbFile;
  }
}
