import DatabaseAccessException from './DatabaseAccessException';

/**
 * Erreur lancée lors d'une erreur durant la lecture de la base de donnée.
 *
 * @class
 *
 * @extends {DatabaseAccessException} - Étend la classe d'erreur générale d'accès à la base de
 *   donnée.
 *
 * @author Paul-Louis Mas
 */
export default class DatabaseReadException extends DatabaseAccessException {
  /**
   * @param {String} dbFile - Le fichier de base de donnée.
   * @param {any[]} args - Les arguments de l'erreur originale.
   */
  constructor(dbFile: string, ...args: string[]) {
    super(dbFile, ...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DatabaseReadException);
    }

    /** Le nom de l'erreur */
    this.name = 'DatabaseReadException';

    /** Le fichier de base de donnée */
    this.dbFile = dbFile;
  }
}
