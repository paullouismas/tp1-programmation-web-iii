import DatabaseException from './DatabaseException';

/**
 * Erreur lancée lors de la tentative de parser du JSON mal formé.
 *
 * @class
 *
 * @extends {DatabaseException} - Étend la classe d'erreur général de base de donnée.
 *
 * @author Paul-Louis Mas
 */
export default class DatabaseJSONParseException extends DatabaseException {
  /**
   * @param {String} dbFile - Le fichier de base de donnée.
   * @param {any[]} args - Les arguments de l'erreur originale.
   */
  constructor(dbFile: string, ...args: string[]) {
    super(dbFile, ...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DatabaseJSONParseException);
    }

    /** Le nom de l'erreur */
    this.name = 'DatabaseJSONParseException';

    /** Le fichier de base de donnée */
    this.dbFile = dbFile;
  }
}
