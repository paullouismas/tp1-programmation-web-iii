import DatabaseInitException from './DatabaseInitException';

/**
 * Erreur lancée lors d'une erreur durant la création du fichier de la base de donnée.
 *
 * @class
 *
 * @extends {DatabaseInitException} - Étend la classe d'erreur général d'initialisation de base de
 *   donnée.
 *
 * @author Paul-Louis Mas
 */
export default class DatabaseWriteFileException extends DatabaseInitException {
  /**
   * @param {String} dbFile - Le fichier de base de donnée.
   * @param {any[]} args - Les arguments de l'erreur originale.
   */
  constructor(dbFile: string, ...args: string[]) {
    super(dbFile, ...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DatabaseWriteFileException);
    }

    /** Le nom de l'erreur */
    this.name = 'DatabaseWriteFileException';

    /** Le fichier de base de donnée */
    this.dbFile = dbFile;
  }
}
