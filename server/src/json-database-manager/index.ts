// #region Import des modules

import fs from 'fs/promises';
import path from 'path';

import BetterJSONSerializer from '../better-json-serializer';
import DatabaseCircularJSONException from './DatabaseCircularJSONException';
import DatabaseJSONParseException from './DatabaseJSONParseException';
import DatabasePredicateException from './DatabasePredicateException';
import DatabaseReadException from './DatabaseReadException';
import DatabaseStructureCreationException from './DatabaseStructureCreationException';
import DatabaseWriteException from './DatabaseWriteException';
import DatabaseWriteFileException from './DatabaseWriteFileException';
import { IInit } from './Init';

// #endregion

// #region Définition de la classe principale

/**
 * Classe permettant d'interagir avec une base de donnée au format JSON.
 *
 * @class
 *
 * @author Paul-Louis Mas
 */
class JSONDatabaseManager<D> {
  /** Le fichier de base de donnée */
  public dbFile: string;

  /** Si la base de donnée doit être formatée lors de la sauvegarde de données */
  private prettyPrintContent: boolean;

  /**
   * @param {String} [dbFile] - Le fichier de la base de donnée.
   * @param {Boolean} [prettyPrintContent=false] - Si la base de donnée doit être formatée lors de
   *   la sauvegarde de données.
   */
  public constructor(dbFile: string, prettyPrintContent = false) {
    /** Le fichier de base de donnée */
    this.dbFile = dbFile;

    /** Si la base de donnée doit être formatée lors de la sauvegarde de données */
    this.prettyPrintContent = prettyPrintContent;
  }

  /**
   * Initialise la base de donnée en la créant si besoin.
   *
   * @param {Object} config - La configuration de la base de donnée.
   * @param {Object} [config.defaultDatabaseContent={}] - Le contenue par défaut à écrire dans la
   *   base de donnée si elle n'existe pas encore.
   * @param {Boolean} [config.clearDatabaseOnStartup=false] - Si la base de donnée doit être vidé
   *   lorsque la connection est effectuée.
   *
   * @returns {Promise<void>} Une `Promise` dont l'état indique le succès ou l'erreur de
   *   l'initialisation de la base de données.
   *
   * @throws {DatabaseStructureCreationException} - Lance une erreur si la structure de dossier
   *   pour créer la base de donnée ne peut être créé.
   * @throws {DatabaseWriteFileException} - Lance une erreur si le fichier de la base de donnée
   *   ne peut être créé.
   */
  public async init({
    defaultDatabaseContent = {},
    clearDatabaseOnStartup = false,
  }: IInit): Promise<void> {
    /** Le contenue par défaut de la base de donnée */
    const defaultFileContent = BetterJSONSerializer.stringify(
      defaultDatabaseContent,
      this.prettyPrintContent ? 2 : 0,
    );

    try {
      // Vérifie si le fichier de la base de donnée existe,
      // une erreur est lancée s'il n'existe pas
      await fs.stat(this.dbFile);

      try {
        if (clearDatabaseOnStartup) {
          await fs.writeFile(this.dbFile, defaultFileContent, { encoding: 'utf-8' });
        }
      } catch (error) {
        // Impossible d'overwrite le contenue de la base de donnée
        console.error(error);

        // Rejette la Promise avec une erreur appropriée
        return Promise.reject(new DatabaseWriteFileException(this.dbFile, error));
      }
    } catch (_error) {
      // La base de donnée n'existe pas, il faut la créer
      try {
        // Crée la structure de dossiers si nécessaire
        await fs.mkdir(path.dirname(this.dbFile), { recursive: true });

        try {
          // Crée le fichier de la base de donnée
          await fs.writeFile(this.dbFile, defaultFileContent, { encoding: 'utf-8' });

          console.log('Created database file at: ', this.dbFile);
        } catch (error) {
          // Impossible de créer la base de donnée
          console.error('Cannot create database file.', error);

          // Rejette la Promise avec une erreur appropriée
          return Promise.reject(new DatabaseWriteFileException(this.dbFile, error));
        }
      } catch (error) {
        // Impossible de créer la structure de dossiers
        console.error('Cannot create directory structure.', error);

        // Rejette la Promise avec une erreur appropriée
        return Promise.reject(new DatabaseStructureCreationException(this.dbFile, error));
      }
    }

    // Résout la Promise
    return Promise.resolve();
  }

  /**
   * Utilise un prédicat pour accéder à du contenue dans la base de donnée.
   *
   * @param {(dbContent: D) => any} [predicate=(dbContent) => dbContent] - Le prédicat permettant
   *   d'accéder au contenue dans la base de données.
   *
   * @returns {Promise<any>} Une `Promise` dont le contenue est le contenue accédé depuis le
   *   prédicat.
   *
   * @throws
   */
  public async get<O extends D | unknown>(
    predicate: (dbContent: D) => O = (dbContent) => dbContent as O,
  ): Promise<O> {
    try {
      /** Le contenue du fichier de la base de donnée */
      const fileContent = await fs.readFile(this.dbFile, { encoding: 'utf-8' });

      try {
        /** Le contenue parsé de la base de donnée */
        const dbContent = BetterJSONSerializer.parse<D>(fileContent);

        try {
          /** Le contenue de la base de donnée accédé depuis le prédicat */
          const content = predicate(dbContent) as O;

          // Retourne le contenue dans la Promise résolu
          return Promise.resolve(content);
        } catch (error) {
          // Erreur dans le prédicat
          return Promise.reject(new DatabasePredicateException(this.dbFile, error));
        }
      } catch (error) {
        // Impossible de parser le contenue de la base de donnée
        return Promise.reject(new DatabaseJSONParseException(this.dbFile, error));
      }
    } catch (error) {
      // Impossible de lire le contenue de la base de donnée
      return Promise.reject(new DatabaseReadException(this.dbFile, error));
    }
  }

  /**
   * Met à jour la base de donnée selon le prédicat.
   *
   * @param {(dbContent: D) => any} [predicate=(dbContent) => dbContent] - Le prédicat permettant
   *   de mettre à jour la base de donnée.
   *
   * @returns {Promise<void>} Une `Promise` dont l'état indique le succès ou l'erreur de la mise à
   *   jour de la base de donnée.
   *
   * @throws
   */
  public async set(predicate: (dbContent: D) => D = (dbContent) => dbContent): Promise<void> {
    try {
      /** Le contenue de la base de donnée */
      const dbContent = (await this.get((databaseContent) => databaseContent)) as D;

      try {
        /** Le contenue de la base de donnée mis à jour depuis le prédicat */
        const updatedDbContent = predicate(dbContent);

        try {
          /** Le contenue mis à jour de la base de donnée */
          const fileContent = BetterJSONSerializer.stringify<D>(
            updatedDbContent,
            this.prettyPrintContent ? 2 : 0,
          );

          try {
            // Écris le contenue mis à jour dans la base de donnée
            await fs.writeFile(this.dbFile, fileContent, { encoding: 'utf-8' });

            // Résout la Promise
            return Promise.resolve();
          } catch (error) {
            return Promise.reject(new DatabaseWriteException(this.dbFile, error));
          }
        } catch (error) {
          // Impossible de convertir en string: JSON circulaire
          return Promise.reject(new DatabaseCircularJSONException(this.dbFile, error));
        }
      } catch (error) {
        // Erreur dans le prédicat de mise à jour
        return Promise.reject(new DatabasePredicateException(this.dbFile, error));
      }
    } catch (error) {
      // Impossible de récupérer le contenue de la base de donnée
      return Promise.reject(new DatabaseReadException(this.dbFile, error));
    }
  }
}

// #endregion

export default JSONDatabaseManager;
