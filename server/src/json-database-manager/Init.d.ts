export interface IInit {
  defaultDatabaseContent: Record<string, unknown>;
  clearDatabaseOnStartup: boolean;
}
