/**
 * Erreur générale lors d'une erreur dans la base de donnée.
 *
 * @class
 *
 * @extends {Error} - Étend la classe d'erreur de base.
 *
 * @author Paul-Louis Mas
 */
export default class DatabaseException extends Error {
  /** Le fichier de base de donnée */
  public dbFile: string;

  /**
   * @param {String} dbFile - Le fichier de base de donnée.
   * @param {any[]} args - Les arguments de l'erreur originale.
   */
  constructor(dbFile: string, ...args: string[]) {
    super(...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DatabaseException);
    }

    /** Le nom de l'erreur */
    this.name = 'DatabaseException';

    /** Le fichier de base de donnée */
    this.dbFile = dbFile;
  }
}
