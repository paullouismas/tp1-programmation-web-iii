import DatabaseException from './DatabaseException';

/**
 * Erreur lancée lors d'une tentative de conversion de JSON circulaire en chaîne de caractères.
 *
 * @class
 *
 * @extends {DatabaseException} - Étend la classe d'erreur général de base de donnée.
 *
 * @author Paul-Louis Mas
 */
export default class DatabaseCircularJSONException extends DatabaseException {
  /**
   * @param {String} dbFile - Le fichier de base de donnée.
   * @param {any[]} args - Les arguments de l'erreur originale.
   */
  constructor(dbFile: string, ...args: string[]) {
    super(dbFile, ...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DatabaseCircularJSONException);
    }

    /** Le nom de l'erreur */
    this.name = 'DatabaseCircularJSONException';

    /** Le fichier de base de donnée */
    this.dbFile = dbFile;
  }
}
