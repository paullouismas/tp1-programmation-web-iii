import DatabaseException from './DatabaseException';

/**
 * Erreur générale lors d'une erreur durant l'accès à la base de donnée.
 *
 * @class
 *
 * @extends {DatabaseException} - Étend la classe d'erreur générale de base de donnée.
 *
 * @author Paul-Louis Mas
 */
export default class DatabaseAccessException extends DatabaseException {
  /**
   * @param {String} dbFile - Le fichier de base de donnée.
   * @param {any[]} args - Les arguments de l'erreur originale.
   */
  constructor(dbFile: string, ...args: string[]) {
    super(dbFile, ...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DatabaseAccessException);
    }

    /** Le nom de l'erreur */
    this.name = 'DatabaseAccessException';

    /** Le fichier de base de donnée */
    this.dbFile = dbFile;
  }
}
