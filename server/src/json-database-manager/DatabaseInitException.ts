import DatabaseException from './DatabaseException';

/**
 * Erreur générale lors d'une erreur durant l'initialisation de la base de donnée.
 *
 * @class
 *
 * @extends {DatabaseException} - Étend la classe d'erreur générale de base de donnée.
 *
 * @author Paul-Louis Mas
 */
export default class DatabaseInitException extends DatabaseException {
  /**
   * @param {String} dbFile - Le fichier de base de donnée.
   * @param {any[]} args - Les arguments de l'erreur originale.
   */
  constructor(dbFile: string, ...args: string[]) {
    super(dbFile, ...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DatabaseInitException);
    }

    /** Le nom de l'erreur */
    this.name = 'DatabaseInitException';

    /** Le fichier de base de donnée */
    this.dbFile = dbFile;
  }
}
