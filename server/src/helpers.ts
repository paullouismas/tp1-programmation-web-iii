/**
 * Fonction pour trier une liste d'objets selon une de leurs propriétés.
 *
 * @param {Object[]} array - Une liste d'objets à trier.
 * @param {String} propertyName - Le nom de la propriété commune aux objets sur laquel les trier.
 * @param {Boolean} [ascending=true] - Si les objets doivent être triés par ordre ascendant.
 *
 * @returns {Object[]} - La liste d'objets, triés selon une propriété.
 *
 * @author Paul-Louis Mas
 */
const sortBy: <O extends Record<string, unknown>>(
  array: O[],
  propertyName: string,
  ascending: boolean,
) => O[] = (array, propertyName, ascending = true) => {
  /** La liste d'objets triés selon [`propertyName`]{@link propertyName} */
  const sorted = array.sort((item1, item2) => {
    const property1 = item1[propertyName];
    const property2 = item2[propertyName];

    if (property1 < property2) {
      return ascending ? -1 : 1;
    }

    if (property1 > property2) {
      return ascending ? 1 : -1;
    }

    return 0;
  });

  return sorted;
};

export {
  // eslint-disable-next-line
  sortBy,
};
