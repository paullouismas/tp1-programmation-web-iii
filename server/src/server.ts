// #region Import des modules

import path from 'path';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import Joi, { ValidationError } from 'joi';
import dotenv from 'dotenv';

import JSONDatabaseManager from './json-database-manager';
import { sortBy } from './helpers';
import { ITodo } from './Todo';

// #endregion

// Initialise les variables d'environnement
dotenv.config({ path: path.resolve(process.cwd(), '../.env') });

// #region Définition des constantes

/** L'hôte utilisé par le serveur */
const HOSTNAME = process.env.SERVER_HOSTNAME || 'localhost';
/** Le port utilisé par le serveur */
const PORT = Number.parseInt(process.env.SERVER_PORT, 10) || 3000;

/** Le schéma d'une tâche */
const todoSchema = Joi.object<ITodo>({
  id: Joi.string()
    .trim()
    .regex(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/)
    .required(),
  completed: Joi.boolean().required(),
  dueDate: Joi.string()
    .trim()
    .regex(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T([01][0-9]|2[0-3]):([0-5][0-9])$/)
    .allow(null)
    .required(),
  name: Joi.string().trim().required(),
  description: Joi.string().allow('').required(),
});

/** L'instance d'accès à la base de donnée */
const db = new JSONDatabaseManager<{ todos: Map<string, ITodo> }>(
  path.join(__dirname, '../db/todos.db.json'),
  true,
);

/** L'instance du serveur */
const app = express();
/** Le routeur principal */
const router = express.Router();

// #endregion

// #region Définition des middlewares

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(cors());
// Middleware pour logger
app.use((request, _response, next) => {
  // Log la requête
  console.log(`${request.method}: ${request.originalUrl}`);

  // Appelle le prochain middleware
  next();
});

// #endregion

// #region Définition des routes du routeur

// CRUD@Create - POST /todos/
router.post('/todos/', async (request, response) => {
  try {
    /** La tâche validé par le schéma */
    const validatedTodo = (await todoSchema.validateAsync(request.body)) as ITodo;

    try {
      // Ajoute la tâche dans la DB
      await db.set((dbContent) => {
        dbContent.todos.set(validatedTodo.id, validatedTodo);

        return dbContent;
      });

      // Retourne une réponse indiquant que la tâche a été créé
      response.status(201).json(validatedTodo);
    } catch (error) {
      // Impossible d'ajouter la tâche
      console.error(error);

      // Retourne une réponse d'erreur
      response.status(500).json({ error: 'Unable to update database.' });
    }
  } catch (error) {
    // Le schéma de la tâche est invalide
    console.error(error);

    // Retourne une réponse d'erreur
    response
      .status(400)
      .json({ error: (error as ValidationError).details.map(({ message }) => message) });
  }
});

// CRUD@Read - GET /todos/
router.get('/todos/', async (request, response) => {
  /** Le filtre textuel */
  const filter = String(request.query.filter || '');
  /** L'ordre de tri */
  const order = String(request.query.order || '');
  /** Le nombre de tâches maximum /page */
  const limit = Number.parseInt(request.query.limit as string, 10) || 20;
  /** Le numéro de page */
  const page = Number.parseInt(request.query.page as string, 10) || 1;

  try {
    /** La liste de tâches filtrés selon le filtre textuel */
    const filteredTodos = await db.get<ITodo[]>(({ todos }) =>
      Array.from(todos.values()).filter(({ name }) => {
        /** Si la tâche matche le filtre */
        const match = filter ? name.toLowerCase().indexOf(filter.toLowerCase()) >= 0 : true;

        return match;
      }),
    );
    /** La liste ordonnée */
    const orderedTodos = ['name', 'completed'].includes(order)
      ? sortBy<ITodo>(filteredTodos, order, true)
      : filteredTodos;

    /** L'index de début pour l'extraction d'une sous-liste */
    const indexStart = limit * (page - 1);
    /** La sous-liste de tâches extraite */
    const returnedTodos = orderedTodos.slice(indexStart, indexStart + limit);
    /** La dernière page de la pagination */
    const paginationLast = Math.ceil(orderedTodos.length / limit) || 1;
    /** Si la sous-liste est tronquée */
    const truncated = orderedTodos.length > returnedTodos.length;

    // Retourne le code HTTP et l'objet de retour
    response.status(truncated ? 206 : 200).json({
      /** Le nombre de tâches au complet */
      resultCount: orderedTodos.length,
      truncated,
      /** La pagination */
      pagination: {
        /** La 1er page */
        first: 1,
        /** La page précédente */
        previous: page > 1 ? page - 1 : null,
        /** La page actuelle */
        current: page,
        /** La page suivante */
        next: page < paginationLast ? page + 1 : null,
        /** La dernière page */
        last: paginationLast,
      },
      /** La sous-liste de tâches */
      todos: returnedTodos,
    });
  } catch (error) {
    // Une erreur est survenue
    console.error(error);

    // Retourne un code d'erreur
    response.status(500).json({ error: 'Unable to access database.' });
  }
});

// CRUD@Read - GET /todos/:id
router.get('/todos/:id', async (request, response) => {
  /** L'id de la tâche */
  const { id } = request.params;

  try {
    /** La tâche associé à cette id */
    const todo = await db.get<ITodo | undefined>(({ todos }) => todos.get(id));

    if (todo) {
      // Retourne la tâche
      response.status(200).json(todo);
    } else {
      // La tâche n'existe pas, retourne une erreur
      response.status(404).json({ error: `Todo '${id}' does not exist.` });
    }
  } catch (error) {
    // Une erreur est survenue
    console.error(error);

    // Retourne une erreur
    response.status(500).json({ error: 'Unable to access database.' });
  }
});

// CRUD@Update - PUT /todos/:id
router.put('/todos/:id', async (request, response) => {
  try {
    /** La tâche validé par le schéma */
    const validatedTodo = (await todoSchema.validateAsync(request.body)) as ITodo;

    /** L'id de la tâche à mettre à jour */
    const { id } = request.params;

    try {
      await db.set((dbContent) => {
        if (dbContent.todos.has(id) && dbContent.todos.get(id).id === validatedTodo.id) {
          // Met à jour la référence de la tâche
          dbContent.todos.set(id, validatedTodo);

          response.status(200).json(validatedTodo);
        } else {
          // La tâche n'existe pas, retourne une erreur
          response.status(404).json({ error: `Todo '${id}' does not exist.` });
        }

        return dbContent;
      });
    } catch (error) {
      // Une erreur est survenue
      console.error(error);

      // Retourne une erreur
      response.status(500).json({ error: 'Unable to access database.' });
    }
  } catch (error) {
    // Le schéma de la tâche est invalide
    console.error(error);

    // Retourne une réponse d'erreur
    response
      .status(400)
      .json({ error: (error as ValidationError).details.map((detail) => detail.message) });
  }
});

// CRUD@Delete - DELETE /todos/:id
router.delete('/todos/:id', async (request, response) => {
  /** L'id de la tâche à supprimer */
  const { id } = request.params;

  try {
    await db.set((dbContent) => {
      if (dbContent.todos.has(id)) {
        // La tâche existe
        // Retire la tâche
        dbContent.todos.delete(id);

        response.status(204).json({});
      } else {
        // La tâche n'existe pas, retourne une erreur
        response.status(404).json({ error: `Todo '${id}' does not exist.` });
      }

      return dbContent;
    });
  } catch (error) {
    // Une erreur est survenue
    // Retourne une erreur
    response.status(500).json({ error: 'Unable to access database.' });
  }
});

// #endregion

// Configure l'API sur l'url /api
app.use('/api', router);

// Initialise la base de donnée et s'y connecte
db.init({
  defaultDatabaseContent: {
    todos: new Map<string, ITodo>(),
  },
  clearDatabaseOnStartup: false,
})
  .then(() => {
    // La base de donnée est initialisé, démarre le server
    app.listen(PORT, HOSTNAME, () => {
      console.log(`Server is running at ${HOSTNAME}:${PORT}`);
    });
  })
  .catch((error) => {
    // La base de donnée n'a pu être initialisé
    console.error('Database could not be initialized.', error);
  });
