# TP1 programmation web III

---

TP1 du cours de Programmation Web III technologies et prospective.

Réalisé par:

- François Morel
- Lincey Jérôme
- [Paul-Louis Mas](https://paullouismas.github.io/)
- Simon Castonguay

---

## Table des matières

- [Table des matières](#table-des-matières)
- [Présentation du projet](#présentation-du-projet)
- [Utilisation du projet](#utilisation-du-projet)
  - [Démarrer le projet](#démarrer-le-projet)
  - [Modifier l'hôte et/ou le port](#modifier-lhôte-etou-le-port)
    - [Serveur](#pour-modifier-le-serveur)
    - [Client](#pour-modifier-le-client)
- [Répartition des rôles dans le projet](#répartition-des-rôles-dans-le-projet)
- [Fonctionnement du projet serveur](#fonctionnement-du-projet-serveur)
- [Fonctionnement du projet client](#fonctionnement-du-projet-client)
- [Implémentation du CRUD](#implémentation-du-crud)
  - [CRUD serveur](#crud-serveur)
  - [CRUD client](#crud-client)
- [Dépendances du projet](#dépendances-du-projet)
  - [Dépendances serveur](#dépendances-serveur)
  - [Dépendances client](#dépendances-client)

---

## Présentation du projet

La solution implémente une application de gestion de tâches (Todo list), la solution est séparé en deux (2) projets:

- Le serveur (`server/`)
- Le client (`client/`)

---

## Utilisation du projet

### Démarrer le projet

```shell
# Clone la solution
git clone https://gitlab.com/paullouismas/tp1-programmation-web-iii.git

# Accède au dossier de la solution
cd tp1-programmation-web-iii/

# Exécute le script NodeJS principale
npm run start
```

Cela va installer toutes les dépendances de la solution puis démarrer le serveur et le client dans deux (2) processus différents (cette opération peut prendre un certain temps à s'éxecuter lors de la première exécution).

### Modifier l'hôte et/ou le port

Le serveur tourne normalement à l'adresse `http://localhost:3000/api/`. Si pour une raison ou une autre, il faut le changer d'hôte et/ou de port, il faudra modifier le serveur **ET** le client.

#### Pour modifier le serveur

Créez un fichier `.env` à la racine de la solution et ajoutez les valeurs `SERVER_HOSTNAME` et `SERVER_PORT` pour overwrite les valeurs par défaut:

```env
SERVER_HOSTNAME=localhost
SERVER_PORT=3000
```

#### Pour modifier le client

Ouvrir `client/src/store/index.ts` et modifier la constante `API_BASE` en accordance avec l'hôte et le port définis dans le serveur.

---

## Répartition des rôles dans le projet

- François Morel

  - Création du composant Create du CRUD (`client/src/views/create/CreateTask.vue`)
  - Implémentation de la logique de création d'une tâche dans le Store avec l'action `createTodo`

- Lincey Jérôme

  - Création de la classe Todo (`client/src/models/Todo.ts`) qui représente une tâche au sein de la solution
  - Création du composant Delete du CRUD (`client/src/views/delete/DeleteTask.vue`)
  - Implémentation de la logique de suppression d'une tâche dans le Store avec l'Action `deleteTodo`

- Paul-Louis Mas

  - Configuration du projet Git
  - Création de la solution et de l'architecture des projets
  - Création et configuration du serveur
  - Création de la base du Store avec [Vuex](https://vuex.vuejs.org/) pour effectuer des requêtes à l'API exposé par le serveur
  - Création du routage du client avec [vue-router](https://router.vuejs.org/)
  - Création des composants réutilisable dans le projet client (`client/src/components/`)
  - Création des modèles de données dans le projet client (`client/src/models/`)
  - Création du composant Read du CRUD (`client/src/views/read/ReadTask.vue`) ainsi que du composant d'accueil qui affiche la liste au format réduit des tâches (`client/src/views/tasklist/TaskList.vue`)
  - Écriture de la documentation de la solution
  - Implémentation de la logique d'accès aux tâches (tâche unique, ainsi que la liste de toutes les tâche) dans le Store avec les Actions `loadTodos` et `getTodo`

- Simon Castonguay

  - Création du composant Update du CRUD (`client/src/views/update/UpdateTask.vue`)
  - Implémentation de la logique de mise à jour d'une tâche dans le Store avec l'Action `updateTodo`

---

## Fonctionnement du projet serveur

Le projet serveur (`server/`) est basé sur le module [express](https://www.npmjs.com/package/express) pour implémenter l'API, les schémas de données sont assurés avec [joi](https://www.npmjs.com/package/joi).
Un module "custom" (fait maison) est utilisé pour accéder et manipuler la base de donnée au format JSON. Puisque la base de donnée utilise des types de données non-standard pour stocker des données (utilisation des objets Map), un second module fait maison est utilisé pour sérialiser et désérialier ces objets en JSON. (voir [Dépendances serveur](#dépendances-serveur))

---

## Fonctionnement du projet client

Le projet client (`client/`) utilise le Framework [Vue CLI](https://cli.vuejs.org/) comme structure de base pour l'organisation du projet. Il utilise le plugin [Vuex](https://vuex.vuejs.org/) pour la manipulation des données avec le Store (qui est lié à l'API) ainsi que le plugin [vue-router](https://router.vuejs.org/) pour effectuer le routage. L'interface utilisateur est construit avec le framework CSS [Bulma](https://bulma.io/) qui est basé sur le language [SCSS](https://sass-lang.com/documentation/syntax#scss).

---

## Implémentation du CRUD

La solution implémente le fonctionnement du CRUD, que ce soit au niveau du serveur, mais aussi au niveau du client.

### CRUD serveur

| Méthode HTTP | URL                | Corps de la requête                             | Rôle CRUD | Description                                      |
| :----------- | :----------------- | :---------------------------------------------- | :-------- | :----------------------------------------------- |
| POST         | `/api/todos/`      | La tâche à créer au format `JSON`               | Create    | Crée une nouvelle tâche                          |
| GET          | `/api/todos/`      | Des propriétés de filtre pour trier les données | Read      | Récupère la liste de toutes les tâches           |
| GET          | `/api/todos/<ID>/` |                                                 | Read      | Récupère une tâche en particulier, depuis son id |
| PUT          | `/api/todos/<ID>/` | La tâche à mettre à jour au format `JSON`       | Update    | Modifie une tâche en particulier, depuis son id  |
| DELETE       | `/api/todos/<ID>/` |                                                 | Delete    | Supprime une tâche en particulier, depuis son id |

### CRUD client

| Composant                                | URL                       | Rôle CRUD | Description                                                         |
| :--------------------------------------- | :------------------------ | :-------- | :------------------------------------------------------------------ |
| `client/src/views/create/CreateTask.vue` | `/create/`                | Create    | Page pour créer une tâche                                           |
| `client/src/views/tasklist/TaskList.vue` | `/` <br /><br /> `/list/` | Read      | Page d'accueil, pour afficher la liste des tâches, au format réduit |
| `client/src/views/read/ReadTask.vue`     | `/read/<ID>/`             | Read      | Page pour afficher une tâche en détails, depuis son id              |
| `client/src/views/update/UpdateTask.vue` | `/update/<ID>/`           | Update    | Page pour modifier une tâche, depuis son id                         |
| `client/src/views/delete/DeleteTask.vue` | `/delete/<ID>/`           | Delete    | Page pour supprimer une tâche, depuis son id                        |

---

## Dépendances du projet

### Dépendances serveur

| Dépendance                                                                                                                                        | Utilisation                                                                                                 |
| :------------------------------------------------------------------------------------------------------------------------------------------------ | :---------------------------------------------------------------------------------------------------------- |
| [express](https://www.npmjs.com/package/express/)                                                                                                 | Montage de l'API                                                                                            |
| [cors](https://www.npmjs.com/package/cors/)                                                                                                       | Permet CORS dans l'API                                                                                      |
| [body-parser](https://www.npmjs.com/package/body-parser/)                                                                                         | Utilisé pour sécuriser les données venant de l'extérieur dans l'API                                         |
| [joi](https://www.npmjs.com/package/joi/)                                                                                                         | Utiliser pour assurer l'intégrité des schémas de donnée                                                     |
| [better-json-serializer](https://gitlab.com/paullouismas/tp1-programmation-web-iii/-/tree/main/server/src/better-json-serializer) (module custom) | Module custom pour faciliter la sérialization et désérialization d'objets non standards en JSON (ex: `Map`) |
| [json-database-manager](https://gitlab.com/paullouismas/tp1-programmation-web-iii/-/tree/main/server/src/json-database-manager) (module custom)   | Module custom pour manipuler un fichier JSON en tant que base de donnée                                     |

### Dépendances client

| Dépendance                                                                                                                                                                                                                                                                                                                                                                                                                                            | Utilisation                                                       |
| :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :---------------------------------------------------------------- |
| [@creativebulma/bulma-badge](https://www.npmjs.com/package/@creativebulma/bulma-badge/)                                                                                                                                                                                                                                                                                                                                                               | Utilisé pour afficher un badge sur les formats réduits des tâches |
| [@fortawesome/fontawesome-svg-core](https://www.npmjs.com/package/@fortawesome/fontawesome-svg-core/) <br /><br /> [@fortawesome/free-regular-svg-icons](https://www.npmjs.com/package/@fortawesome/free-regular-svg-icons) <br /><br /> [@fortawesome/free-solid-svg-icons](https://www.npmjs.com/package/@fortawesome/free-solid-svg-icons) <br /><br /> [@fortawesome/vue-fontawesome](https://www.npmjs.com/package/@fortawesome/vue-fontawesome) | Utilisation des icônes FontAwesome dans VueJS                     |
| [axios](https://www.npmjs.com/package/axios/)                                                                                                                                                                                                                                                                                                                                                                                                         | Pour effectuer les requêtes HTTP vers l'API                       |
| [Bulma](https://www.npmjs.com/package/bulma/)                                                                                                                                                                                                                                                                                                                                                                                                         | Framework CSS pour les interfaces visuels                         |
| [uuid](https://www.npmjs.com/package/uuid/)                                                                                                                                                                                                                                                                                                                                                                                                           | Génération de UUIDs uniques pour les propriétés id des tâches     |
| [vue](https://www.npmjs.com/package/vue/)                                                                                                                                                                                                                                                                                                                                                                                                             | Framework JavaScript                                              |
| [vue-router](https://www.npmjs.com/package/vue-router/)                                                                                                                                                                                                                                                                                                                                                                                               | Routage natif de VueJS                                            |
| [vuex](https://www.npmjs.com/package/vuex/)                                                                                                                                                                                                                                                                                                                                                                                                           | Store natif de VueJS                                              |
